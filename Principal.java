public class Principal {
    
    public static void main (String[] args) {
        Producto producto1 = new Producto();
        Producto producto2 = new Producto();

        producto1.setCodigo(123456789L);
        producto1.setNombre("Coca-Cola 3L");
        producto1.setPrecioDeCosto(100.0F);
        producto1.setPorcentajeDeGanancia(25.0F);
        producto1.setIVA(21.0F);
        producto1.calculaPrecioVenta(producto1.getPrecioDeCosto(), producto1.getPorcentajeDeGanancia());
        System.out.println("Producto 1:");
        producto1.MostrarPorPantalla();

        producto2.setCodigo(456831597L);
        producto2.setNombre("Sprite 3L");
        producto2.setPrecioDeCosto(90.0F);
        producto2.setPorcentajeDeGanancia(25.0F);
        producto2.setIVA(21.0F);
        producto2.calculaPrecioVenta(producto2.getPrecioDeCosto(), producto2.getPorcentajeDeGanancia());
        System.out.println("Producto 2:");
        producto2.MostrarPorPantalla();

        if(producto1.getPrecioDeVenta()<producto2.getPrecioDeVenta()){
            System.out.println("El producto "+producto2.getNombre()+" es mas caro");
        }
        else{
            if(producto1.getPrecioDeVenta()>producto2.getPrecioDeVenta()){
                System.out.println("El producto "+producto1.getNombre()+" es mas caro");
            }
            else{
                System.out.println("Ambos productos tienen el mismo precio de venta");
            }
        }
    }
}
