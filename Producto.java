public class Producto {

    private Long codigo;
    private String nombre;
    private Float precioDeCosto;
    private Float porcentajeDeGanancia;
    private Float iva;
    private Float precioDeVenta;

    public void setCodigo(final Long vcodigo) {
        this.codigo = vcodigo;
    }

    public Long setCodigo() {
        return codigo;
    }

    public void setNombre(final String vnombre) {
        this.nombre = vnombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setPrecioDeCosto(final Float vPrecioDeCosto) {
        this.precioDeCosto = vPrecioDeCosto;
    }

    public Float getPrecioDeCosto() {
        return precioDeCosto;
    }

    public void setPorcentajeDeGanancia(final Float vPorcentajeDeGanancia) {
        this.porcentajeDeGanancia = vPorcentajeDeGanancia;
    }

    public Float getPorcentajeDeGanancia() {
        return porcentajeDeGanancia;
    }

    public void setIVA(final Float vIva) {
        this.iva = vIva;
    }

    public Float getIVA() {
        return iva;
    }

    public void calculaPrecioVenta(Float vprecioDeCosto, Float vporcentajeDeGanancia){
        this.precioDeVenta = vprecioDeCosto + vprecioDeCosto*(vporcentajeDeGanancia/100);
    }
    public Float getPrecioDeVenta() {
        return precioDeVenta;
    }

    public void MostrarPorPantalla() {
        System.out.println("Codigo: "+codigo);
        System.out.println("Nombre: "+nombre);
        System.out.println("Precio de Costo: "+precioDeCosto);
        System.out.println("Porcentaje de Ganancia: "+porcentajeDeGanancia);
        System.out.println("Precio de Venta: "+precioDeVenta);
    } 
}